import React from "react";
import type { NextPage } from 'next'
import Head from 'next/head'
import Article from "../../components/Article";
import { getArticleBySlug, getAllArticles } from "../../lib/util";
import { ArticleType } from "../../lib/types";

interface Params {
  params: {
    slug: string
  }
}
export async function getStaticProps({ params }: Params) {
  const article: ArticleType = getArticleBySlug(params.slug);
  return {
    props: { article },
  }
}

export async function getStaticPaths() {
  const articles: Array<ArticleType> = getAllArticles();
  return {
    paths: articles.map((article) => {
      return {
        params: {
          slug: article.meta.slug,
        },
      };
    }),
    fallback: false,
  }  
}

interface Props {
  article: ArticleType
}

const Index: NextPage<Props> = ({ article }) => {
  return (
    <>
      <Head>
        <title>{article.meta.title}</title>
        <meta name="description" content={article.meta.description} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Article article={article} />
    </>
  )
}
export default Index
