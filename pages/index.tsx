import RandomMoonCat from 'components/RandomMoonCat'
import type { NextPage } from 'next'
import Head from 'next/head'
import NextLink from 'next/link'
import ArticleSummary from 'components/ArticleSummary'
import { getAllArticles } from 'lib/util'
import { ArticleType } from 'lib/types'

const ZWS = '\u200B'

export async function getStaticProps() {
  const articles: Array<ArticleType> = getAllArticles(false)
  return {
    props: { latestArticle: articles[0] },
  }
}

interface Props {
  latestArticle: ArticleType
}

const Home: NextPage<Props> = ({ latestArticle }) => {
  const titleString = `MoonCat${ZWS}Rescue Community`
  return (
    <section>
      <Head>
        <title>{titleString}</title>
        <meta
          name="description"
          content="Playground of the colorful felines rescued from the moon"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1 className="hero">{titleString}</h1>
      <section className="card-help">
        <p>MoonCats are an original NFT which launched in 2017 and helped pioneer <strong>on-chain generation</strong>, <strong>fair distribution</strong>, and <strong>user customization</strong>.</p>
      </section>
      <div className="mooncat-row">
        <div><RandomMoonCat /></div>
        <div><RandomMoonCat /></div>
        <div><RandomMoonCat /></div>
      </div>
      <section className="card">
        <h2 className="section-header">Latest news from the Chainstation</h2>
        <ArticleSummary article={latestArticle} />
      </section>
      <section className="card-help">
        <h2>Welcome to the MoonCat{ZWS}Community Website!</h2>
        <p>MoonCat{ZWS}Rescue sprouted an amazing <em>grass-roots community</em> of Rescuers, Adopters, Artists, Programmers, and Enthusiasts. <em>People rose to the challenge of reviving a mission half-finished and almost fully forgotten, and saw it through.</em> Though ponderware initially started the MoonCat{ZWS}Rescue mission, a wonderful community has continued to grow around it.</p>
        <p>The MoonCat{ZWS}Rescue team is grateful to have been welcomed into that community. Together, we hope to expand on the world of MoonCat{ZWS}Rescue, adding content and value to the ecosystem as a whole. We hope to <strong>explore, delight, and educate</strong> in the spirit of the original contract.</p>
        <p>New developments and updates will be posted here. We want to maintain the MoonCat{ZWS}Rescue site as it is now: largely preserved as it was in 2017. Already that copy has grown long in the paw (with its outdated instructions and claims of $0.50 gas), but it doesn't feel right to take it down or substantially alter it; the MoonCat{ZWS}Rescue homepage has received its last update.</p>
        <ul>
          <li><NextLink href="adopt-a-mooncat">Adopt a MoonCat</NextLink>: Info to help you adopt your first MoonCat, or grow the size of your adopted litter!</li>
          <li><NextLink href="play-with-mooncats">Play with MoonCats</NextLink>: Once you have adopted a furry friend, what can you do with them? Resources that MoonCat owners can partake in, and information about MoonCats as an entire collection.</li>
          <li><NextLink href="build-with-mooncats">Build with MoonCats</NextLink>: Dapp developer, wallet creator, or other technically-minded person who wants to peek under the hood and see how it all works? Info here about how to integrate with MoonCats on a technical level.</li>
        </ul>
        <p>Want to see what's coming next for the MoonCat ecosystem from the MoonCat{ZWS}Rescue development team? Check out the roadmap!</p>
      </section>
    </section>
  )
}
export default Home
