import 'styles/globals.scss'
import type { AppProps } from 'next/app'
import NextLink from 'next/link'
import BackgroundStars from 'components/BackgroundStars'

const ZWS = '\u200B'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <nav>
        <div id="logo">
          <NextLink href="/"><a>MoonCat{ZWS}Rescue</a></NextLink>
        </div>
        <div>
          <NextLink href="about">About</NextLink>
        </div>
        <div>
          <NextLink href="log-index">Log</NextLink>
        </div>
      </nav>
      <div id="content-container">
        <Component {...pageProps} />
      </div>
      <BackgroundStars />
    </>
  )
}

export default MyApp
