import type { NextPage } from 'next'
import Head from 'next/head'
import ArticleSummary from 'components/ArticleSummary'
import { getAllArticles } from 'lib/util'
import { ArticleType } from 'lib/types'

const ZWS = '\u200B'

export async function getStaticProps() {
  const articles: Array<ArticleType> = getAllArticles(false)
  return {
    props: { articles },
  }
}

interface Props {
  articles: Array<ArticleType>
}

const ChainstationLog: NextPage<Props> = ({ articles }) => {
  const titleString = `Chainstation Log`
  return (
    <section>
      <Head>
        <title>{titleString}</title>
        <meta
          name="description"
          content="Playground of the colorful felines rescued from the moon"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1 className="hero">{titleString}</h1>
      <section className="card">
        <p>There's plenty going on around Chainstation Alpha. As the Ethereans continue to adventure with the MoonCats, we're keeping logs of their experiences here, for posterity.</p>
        <div className="articles-listing">
          {articles.map((article) => (
            <ArticleSummary key={article.meta.slug} article={article} />
          ))}
        </div>
      </section>
    </section>
  )
}
export default ChainstationLog
