This is the code repository for the main MoonCatRescue website (UI relaunch). This site is designed as a documentation/introduction site for users to learn about the project. It will eventually be deployed as `https://mooncatrescue.com`.

# Development
This application is a [NextJS](https://nextjs.org/) application (so, using [React](https://reactjs.org/) and [Typescript](https://www.typescriptlang.org/) under-the-hood), with Web3 integrations added in ([ethers](https://www.npmjs.com/package/ethers) and [web3modal](https://www.npmjs.com/package/web3modal)).

To run a local development environment, follow the general instructions for [MoonCatRescue development](https://gitlab.com/mooncatrescue/dev-environment). The first time you clone this repository, you'll need to run `npm install` in it, to get the needed javascript packages. Then run:

    docker compse up

That will start a running environment you can then access at http://localhost:25600
