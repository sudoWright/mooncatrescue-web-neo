import React, { useEffect } from 'react'

const RandomMoonCat = (): JSX.Element => {
  let rescueOrder = Math.floor(Math.random() * 25440)
  let label = `MoonCat #${rescueOrder}`
  return (
    <img className="mooncat-image" alt={label} title={label} src={"https://api.mooncat.community/image/" + rescueOrder + "?scale=3&padding=10"} />
  )
}
export default RandomMoonCat
