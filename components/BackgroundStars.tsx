import React, { useRef, useEffect } from 'react'

const BackgroundStars = (): JSX.Element => {
  const bg1 = useRef<HTMLDivElement>(null)
  const bg2 = useRef<HTMLDivElement>(null)

  function positionBackgroundImages() {
    var bodyHeight = Math.max(
      document.body.scrollHeight,
      document.body.offsetHeight
    )
    if (bodyHeight > window.innerHeight) {
      if (bg1.current != null) bg1.current.style.top = window.scrollY * -0.2 + 'px'
      if (bg2.current != null) bg2.current.style.top = window.scrollY * -0.1 + 'px'
    } else {
      if (bg1.current != null) bg1.current.style.top = '0'
      if (bg2.current != null) bg2.current.style.top = '0'
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', positionBackgroundImages)
    return () => {
      window.removeEventListener('scroll', positionBackgroundImages)
    }
  }, [])

  return (
    <div id="background">
      <div ref={bg1} id="small-stars" />
      <div ref={bg2} id="large-stars" />
    </div>
  )
}
export default BackgroundStars
