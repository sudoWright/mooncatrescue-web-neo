import React from 'react'
import { ArticleType } from 'lib/types'
import ReactMarkdown from 'react-markdown'

function interleave(arr: Array<React.ReactNode>, glue: string): Array<React.ReactNode> {
  let final = [];
  for (let i = 0; i < arr.length; i++) {
    final.push(arr[i]);
    if (i < arr.length - 1) {
      final.push(glue)
    }
  }
  return final
}

type Props = {
  article: ArticleType
}

const Article = ({ article }: Props) => {
  if (typeof article.content == 'undefined') {
    console.error(`Article supplied with no content`)
    article.content = ''
  }
  return (
    <div className="article">
      <h1 className="hero">{article.meta.title}</h1>
      <section className="card article-content">
        <p className="article-meta">
          <strong>Logged date:</strong>{' '}
          <a
            target="_blank"
            href={`https://beaconcha.in/epoch/${article.meta.epoch}`}
          >
            Epoch {article.meta.epoch}
          </a><br />
          <strong>Location:</strong> {article.meta.location}<br />
          <strong>Mission Tags:</strong> {interleave(article.meta.tags, ', ')}
        </p>
        <hr />
        <ReactMarkdown>{article.content}</ReactMarkdown>
      </section>
    </div>
  )
}
export default Article
