import React, { ComponentProps } from 'react'
import NextLink from 'next/link'
import { ArticleType } from 'lib/types'
import ReactMarkdown from 'react-markdown'

type Props = {
  article: ArticleType
}

/**
 * Blog articles are written assuming their content is top-level (page-level H1 is their article's title),
 * so headings in the article start at H2 headings. But for summary lists of articles, there's some other top-level
 * heading for the list page (site index, search page, etc.), and titles of the articles in the list are H2s,
 * meaning if using an "excerpt" from the beginning of the article, the headings in the article should start at H3.
 *
 * Therefore, this configuration block is used to transpose the headlines down one level, so the same source file
 * for the article can be used for the summary blurb too.
 */
const headingStepDown: ComponentProps<typeof ReactMarkdown>['components'] = {
  h2: 'h3',
  h3: 'h4',
  h4: 'h5',
  h5: 'h6',
}

const ArticleSummary = ({ article }: Props): JSX.Element => {
  // If the article has an excerpt defined, use that. Otherwise use the meta description for the article
  const articleBlurb =
    typeof article.meta.excerpt != 'undefined' ? (
      <ReactMarkdown components={headingStepDown}>
        {article.meta.excerpt}
      </ReactMarkdown>
    ) : (
      <p>{article.meta.description}</p>
    )

  return (
    <div className="article-summary">
      <h2 style={{ margin: 0 }}>
        <NextLink href={`blog/${article.meta.slug}`}>
          {article.meta.title}
        </NextLink>
      </h2>
      <p className="article-meta" style={{ marginBottom: '1em' }}>
        <strong>Logged date:</strong> Epoch {article.meta.epoch}
      </p>
      {articleBlurb}
      <p style={{ textAlign: 'right' }}><NextLink href={`blog/${article.meta.slug}`}>Read on...</NextLink></p>
    </div>
  )
}
export default ArticleSummary
