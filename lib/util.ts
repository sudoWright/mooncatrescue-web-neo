import fs from 'fs'
import { join } from 'path'
import matter from 'gray-matter'
import { ArticleType, ArticleMetaType } from './types'

const articlesDirectory = join(process.cwd(), 'articles')

/**
 * Open an MDX file and parse it with gray-matter
 * @param slug The filename of the article to fetch
 * @returns GrayMatterFile
 */
export function getRawArticleBySlug(
  slug: string
): matter.GrayMatterFile<string> {
  const fullPath = join(articlesDirectory, `${slug}.md`)
  const fileContents = fs.readFileSync(fullPath, 'utf8')
  return matter(fileContents)
}

/**
 * Scan all the files in the articles folder; each is a Markdown file that should be fetch-able as an article
 */
export function getAllSlugs(): Array<string> {
  return fs.readdirSync(articlesDirectory)
}

const EXCERPT_FLAG = '<!---MORE--->'

function stripHtmlComments(raw: string) {
  return raw.replaceAll(/<!--(.*?)-->/g, '')
}

export function getArticleBySlug(
  slug: string,
  includeContent: boolean = true
): ArticleType {
  const realSlug = slug.replace(/\.md$/, '')
  const { data, content } = getRawArticleBySlug(realSlug)

  // Conform frontmatter to expected meta types
  let meta: ArticleMetaType = {
    slug: realSlug,
    title: data.title,
    epoch: data.epoch,
    location: data.location,
    tags: data.tags,
  }
  if (typeof meta.epoch == 'undefined') {
    console.error('Article has no epoch', data)
    meta.epoch = 0
  }
  if (typeof meta.tags == 'undefined') {
    meta.tags = []
  }
  if (typeof meta.location == 'undefined') {
    meta.location = 'chainstation'
  }

  if (typeof data.description != 'undefined') {
    meta.description = data.description
  }
  
  let excerptEnd = content.indexOf(EXCERPT_FLAG)
  if (excerptEnd > 0) {
    meta.excerpt = stripHtmlComments(content.substring(0, excerptEnd))
  }

  if (includeContent) {
    return {
      content: stripHtmlComments(content),
      meta,
    }
  } else {
    return {
      meta,
    }
  }
}

export function getAllArticles(includeContent?: boolean): Array<ArticleType> {
  return getAllSlugs()
    .map((slug) => getArticleBySlug(slug, includeContent))
    .sort((article1, article2) => article2.meta.epoch - article1.meta.epoch)
}

export function getArticlesByTag(
  tag: string,
  includeContent?: boolean
): Array<ArticleType> {
  return getAllArticles(includeContent).filter((article) => {
    const tags = article.meta.tags ?? []
    return tags.includes(tag)
  })
}

export function getAllTags(): Array<string> {
  const articles = getAllArticles(false)
  const allTags = new Set<string>()
  articles.forEach((article) => {
    const tags = article.meta.tags
    tags.forEach((tag) => allTags.add(tag))
  })
  return Array.from(allTags)
}
