export interface ArticleMetaType {
  slug: string;
  title: string;
  description?: string;
  epoch: number;
  location: string;
  coverImage?: string;
  excerpt?: string;
  tags: Array<string>;
  ogImage?: {
    url: string;
  };
}

export interface ArticleType {
  meta: ArticleMetaType;
  content?: string;
}
