---
title: lootprints (for MoonCats)
epoch: 65056
tags: [ MoonCatRescue,  Mission,  lootprints, Deep Space ]
---

Unfortunately, the MoonCats have learned that the fastest way to get our attention is to wander up the bridge under the pretense of cuddlyness and start *knocking our coffee cups over onto critical control panels.* It didn't take long for us to understand what they were after. We immediately floated a few ideas to the engineering team (actually, **everything floated** for the week it took to drain and clean the artificial gravity and inertial dampener controls).

It happens that Ethereans all over the station had been talking about loot and adventures for weeks, and the MoonCats had taken notice. As you know, MoonCats *love loot,* and have grown dearly *fond of adventure.* One of the station's *Deep Space Project* researchers had a proposal. Their team had already been hard at work desiging **special MoonCat-ready spacecraft**, and the blueprints for multiple ship forms were already complete.

And, with that, we bring you…

> [lootprints](/lootprints)

…just in time to *save the reactor-core* controls from another precariously balanced beverage.

<!---MORE--->

Each lootprint **entitles the owner to a ship** once manufacturing gets underway and the *Deep Space Project launches.* After much pleading (and plastering their office in pouting MoonCat [photos](/photobooth) the ChainStation accountants have agreed to provide *free lootprints to all of the 15935 MoonCats [acclimated](/acclimator) before September 6th!*

If your cat isn't on the list (shame!), don't worry, ship lootprints are *available to any acclimated MoonCat* who requests one in the coming weeks. We strongly encourage all Ethereans to claim the lootprints right away, lest you endure weeks of looking at your MoonCat's furry backside as you get **conspicuously ignored**. Once claimed, the ships computer will distribute ship-types and capacities using an advanced deep-learning algorithm that no one has understood since Steve's accident. Poor Steve. In the process, the MoonCat will be *privately asked to name their future ship* using one of the ramshackle prototype *MoonCatLinguistics* systems we've patched into the station's AI.

After communicating our plans to the *Hero Cats* on the MoonBase, they put forth a generous proposal of their own. Knowing that there is still much work for them to do there, and that they won't be headed for DeepSpace any time soon, they wish to *auction their ships* to fund the further adventures of their post-lunar brethren. In exchange they requested a mere few thousand cans of [Discount Space Tuna](//boutique.mooncat.community/a/446).

Another small step into the cosmos in the [#AgeOfMooncats](https://twitter.com/hashtag/AgeOfMoonCats?src=hashtag_click).

### P. S. A.

ChainStation Operation Policy Addendum `C47x00d658d50b` has been ratified: Effective immediately, *open-top beverages are forbidden* on the bridge and in all engineering sectors. Bring it in a bottle, or leave it behind!
