---
title: The Next Chapter
epoch: 29825
tags: [ MoonCatRescue, Mission ]
---

Welcome to *ChainStation Alpha!*

The [MoonCat​Rescue](https://mooncatrescue.com) mission was a success, but that was **only the beginning.**

<!---MORE--->

We must do all we can to help all the adorable furry friends we rescued adjust to their new life on the blockchain. As we all know, cats (moon or otherwise) aren't big fans of change.

Of course, we must never forget those billions of precious MoonCats left behind, cloistered in the safety of the moonbase caverns, and the Hero Cats who volunteered to lead them. *Though they can never join their brethren on the blockchain, they are not abandoned!*

We’ll need to check in on them from time to time. Until then, there is plenty of work to be done here. We need to learn more about these cats and help them land on their feet.

### The journey continues
