---
title: Chained to the Future
location: chainstation
epoch: 91999
tags: [ Accessories, MoonCat, On-Chain ]
---

At first the puddles of moon vomit were thought to be caused by a combination of expired [space tuna](//boutique.mooncat.community/a/446) and gleeful overindulgence in [MoonCatPop](//pop.mooncat.community) Analysis of these floor rainbows, however, revealed a unique chemical signature our systems matched to *an experimental pixel-solvent* from an abandoned research project.

MoonCats had stolen their way into a decomissioned lab on Level 1, where Ethereans had long ago *experimented with solidity materialization techniques*. There we found [Zer0](https://opensea.io/assets/0xc3f733ca98e0dad0386979eb96fb1722a1a05e69/0), hammer in paw, attempting a retrofit. We later determined that it was these engineering attempts that had caused the leaks of cryonic residue which the MoonCats had been sampling, and regurgitating.

MoonCats were having an existential crisis. They knew they were on-chain, but *they couldn't see themselves on-chain.* Did they exist at all?

<!---MORE--->

Once we understood what they were trying to accomplish, a crack team of specialists joined Zer0's felingineers. It was months of hard work - claws were torn, songs were yowled in camaraderie. And then it was done. The MoonCats lined up and, one by one, were **fully materialized** onto the blockchain.

- [MoonCatReference](https://etherscan.io/address/0x0B78C64bCE6d6d4447e58b09E53F3621f44A2a48#code)
- [MoonCatTraits](https://etherscan.io/address/0x9330BbfBa0C8FdAf0D93717E4405a410a6103cC2#code)
- [MoonCatColors](https://etherscan.io/address/0x2fd7E0c38243eA15700F45cfc38A7a7f66df1deC#code)
- [MoonCatSVGs](https://etherscan.io/address/0xB39C61fe6281324A23e079464f7E697F8Ba6968f#code)
- [MoonCatAccessoryImages](https://etherscan.io/address/0x91CF36c92fEb5c11D3F5fe3e8b9e212f7472Ec14#code)

The MoonCats now nap peacefully, knowing **their legacy is chiseled forever into the solid blocks** of the Ethereum network.

For more info about the MoonCat's On-Chain existence and to query that data yourself, [read on](https://purrse.mooncat.community/on-chain)!
