---
title: Voting Systems Engaged
location: moonbase
epoch: 24525
tags: [ MoonCatRescue, Mission ]
---

No anomalies detected during full diagnostic.

Live voting system status reads: **ONLINE**

<!---MORE--->

The hour has arrived, fellow ethereans, to *come together and decide:* are the remaining Genesis cats better off living out their lives in peace on luna, or should we work together and figure out how to bring them safely home?

The choice is yours. [Vote right meow.](https://mooncatrescue.com/vote)
