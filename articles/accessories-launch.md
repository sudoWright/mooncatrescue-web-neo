---
title: The Purrsuit of Creature Comfort
epoch: 49121
tags: [ Accessories, MoonCat ]
---

Nowhere on the station is safe from the *crescendo of meowing* that has been growing for weeks. Vital equipment has taken a beating from an endless barrage of *objects knocked from shelves* and clogs of rainbow space fur. Since they left their home, they have wandered *naked and toyless* through the vast expanse of chainspace. We now understand what the MoonCats have been crying for, something of their own, something to make this place their home; in a word: [Accessories!](//boutique.mooncat.community)

<!---MORE--->

Our top researchers have been hard at work figuring out how to materialize objects compatible with MoonCat matter streams. Today we get to taste the tuna-fruits of those labors. Yum! But, *we can't make a full tuna salad without you.* MoonCat tastes are as wild and varied as their coats. We need all hands on deck helping to *create a wide and deep selection* of toys, hats, pogo-sticks, moon-mice, and everything else that might appease our fuzzy friends. Thanks to our team of gruff but competent engineers, an [Accessory Designer](/accessory-designer) has been built which **enables anyone** to design Acclimated MoonCat compatible objects without wading through the arcane technical details. It has all the charms you'd expect from an interface built by engineers.

But making accessories isn't enough. There must be a place for MoonCats to see them. Try them on. Replicate them. Wear them. And for that, the *first retail establishment on the ChainStation* is now open for business. The [MoonCat​Boutique!](//boutique.mooncat.community) Everything for the MoonCat on the go, or on that warm spot on top of your computer. Inventory is a bit sparse at the moment, but we're sure that if we all work together we can make it a **one stop shop for MoonCat dreams!**

As I'm sure you've noticed, since Acclimation, the MoonCats are *not quite as comfortable* in the vacuum of space as they once were. They've really taken to breathing, and get rather upset without it. Thankfully, faithful MoonCat friend [Kimonas](https://twitter.com/Moonsoo46452554) has come to the rescue, with a broad selection of form-fitted helmets to ensure maximum kitty comfort when adventuring through the cosmos. For the [brave vanguard of MoonCats](https://www.easyzoom.com/imageaccess/17355bc7c1774b31bffd0261d891c797) who underwent acclimation, who have *struggled against the vacuum* of space the longest, who *led their brethren* into the modern era: we have managed to secure a supply of [7832 Cadet Helmets.](//boutique.mooncat.community/a/1) They can claim them for free at their leisure.

There are *a few more things* you should be aware of. According to our data, MoonCats like sharing approximately half as much as earth cats. As such, they *will not accept used accessories*, and demand everything come straight from the replicator. Testing shows they do enjoy putting on and taking off their accessories, but they *will never part* with an accessory already given to them. We tried it. **It hurt.**

Having caught wind of our plans, MoonCats are racing around the station with excitement. The [Designer](/accessory-designer) and the [Boutique](//boutique.mooncat.community) are waiting. *We are once again calling on you, fellow ethereans, to step up and aid us* in our ongoing quest to please our MoonCat overlords (their words). It's time to take your rightful place in the annals of *MoonCat​Curio history*, as the [#AgeOfMoonCats](https://twitter.com/hashtag/AgeOfMoonCats?src=hashtag_click) continues to unfold.
