---
title: The 24 GMCs
epoch: 33672
tags: [ Acclimator, MoonCat, Genesis ]
description: Twenty-four Genesis MoonCats have been rumored to be joining forces and pooling their adoption papers together. What does that mean for them and the Etherean rescuers?
---

Genesis MoonCats have always been a *special* and *headstrong* breed. You've probably heard the rumors already via a whisper in a corridor, or a quiet poignant purring, and it is indeed true. *Twenty-four of the freshly Acclimated Genesis MoonCats* have determined that they will transcend the traditional MoonCat/Etherean relationship and instead spread their love *across large swaths of the community*. They have pooled their adoption papers, split them into 10,000,000 shards, and offered us all an opportunity to share in their nobility. Still, that wasn't enough. With the *spirit of the rescue mission* fresh like spacetuna on their minds, they demanded that **400 Ethereans** be selected at random to receive a portion of their gift. If you are [among them](https://pastebin.com/EvFsdWBH), you can claim this rare opportunity [over here](https://mooncat.community/gmc24airdrop).

In other news, the [acclimation](https://mooncat.community/acclimator) process continues to proceed smoothly. With gas prices falling, acclimation is more convenient than ever. There is an electricity in the air here on the station (more than what the electrostatic-cat-hair-collectors usually generate!); as, one by one, MoonCats adapt, the future is *looking bright*, and the **possibilities are growing.**
