---
title: UPDATE 2
location: moonbase
epoch: 24179
tags: [ MoonCatRescue, Mission, Vote ]
---

> ## Log Supplemental
> Preliminary telemetry received. The mainnet [voting contract](https://etherscan.io/address/0x1916F482BB9F3523a489791Ae3d6e052b362C777#code) has been *successfully deployed*. Visit the [voting page here](https://mooncatrescue.com/vote) and cast your vote when **voting begins on Saturday.**

With the help of [TechCrunch](//techcrunch.com) we were able to establish a subspace voice-link to MoonCat​Rescue Community Headquarters back on earth. *It was an honor to be able to address the community directly*, and discuss some of the issues we are facing together.

<!---MORE--->

As you know, there has been some question about MoonCat welfare, in which the *fate of the remaining Genesis MoonCats hangs in the balance.* They are a fussy bunch, and it is on us to read their subtle signals and decide what is best for them. We must all come together and *decide their fate with a vote.* The voice of every etherean who interacted with the original [MoonCat​Rescue](https://etherscan.io/address/0x60cd862c9c687a9de49aecdc3a99b74a4fc54ab6) smart contract, or the main  [Wrapped MoonCat​Rescue](https://etherscan.io/token/0x7c40c393dc0f283f318791d746d894ddd3693572) contract **before Block #12047300** must be heard.

Our chief engineer has managed to allocate enough power from critical systems to *beam 5 MoonCats directly to randomly chosen members of the community who participate in the vote.* Of the 5 brave MoonCats who volunteered (only minor catnip bribery was involved), *four are Rescues* and one is a *Genesis cat* who wouldn’t take no for an answer.

As you read this, satellites are being deployed in preparation to receive the incoming votes. By our estimates, they will activate on **Saturday, March 20, at 12:00 UTC Earth Time.** Once live, we will only be able to keep the channel open for **48 hours**, for space reasons.

More details to follow. For now, be sure your [MetaMask](//metamask.io) systems are fully charged and operational.
