---
title: MoonCatPop
epoch: 75065
tags: [ MoonCatRescue, Ethereans MoonCatPop ]
---

Commerce is humming along here on the station. Just last night a strange ship requested emergency clearance to dock in bay 4. We were relieved (and some other things) to learn their only distress was that they had **"a deal so great it just couldn't wait!"**

Apparently some sort of interstellar beverage vendor, they came to offer MoonCats and their pet Ethereans a *sparkling opportunity*. We tried to shoo them out, but word travels fast. Before we had a chance to show them to the airlock we were all knee-deep in a sea of purring. So, it seems that 256 MoonCats will be *offered the position of SpokesCat* for their own flavor of:

> [MoonCatPop](//pop.mooncat.community)

Space-thirst, *prepare to be quenched!*

<!---MORE--->

A few MoonCats (including that rascal 0) slapped a paw on the dotted line before we could stop them, but *there are still 250 positions left.* So, to distribute them, we are holding a station-wide raffle. Enter your MoonCats today, or wake up with a "present" on your pillow!

### P. S. A.

Please remember to review distress policy guidlines subsection `C`, `A` through `7` before invoking emergency measures.
