---
title: Stalking Their Stuff
epoch: 49546
tags: [ Accessories, MoonCat ]
description: The Boutique is rocking, but how to show off their shiny new helmets, toys, and treasures? Some MoonCats found an abandoned prototype is a corner of the ship that might help!
---

Fellow Ethereans, *your creativity is astounding*; it's been mere days since [The Boutique](//boutique.mooncat.community) opened here at the ChainStation, and already **hundreds of unique products** are stocking the shelves! Needless to say, the *MoonCats are delighted!* Ship operations have been complicated a bit, as we dodge [model rockets](//boutique.mooncat.community/a/307) & [mini lambos](//boutique.mooncat.community/a/169), and wade through piles of [toys](//boutique.mooncat.community/a/427) & [treasures](//boutique.mooncat.community/a/337), but the pain in our feet from stepping on [doge keychains](//boutique.mooncat.community/a/244) is no match for the warmth in our [hearts](//boutique.mooncat.community/a/105). We even had to *tweak the inertial dampeners*, as the resonance of *thousands of purring MoonCats threatened the stations very structure!* Not to mention the airlocks, which have been cycling well past their duty rating, as MoonCats *venture out to test their shiny new [helmets](//boutique.mooncat.community/a/41)*

Unfortunately, MoonCats are never quite satisfied. Even when *chowing down* on their [favorite snack](//boutique.mooncat.community/a/446) It isn't enough to own all these great accessories, they want to **show them off.** We followed the meows to find dozens *rubbing against an abandoned prototype* deep in the bowels of the ship. We told them it wasn't ready, and it might not even work, but *they stood firm.* So we carted it up to the promenade deck and plugged it in. To everyones delight, *it powered right up*. Rough edges and all, the [MoonCatPhotoBooth](/photobooth) is up and running!
