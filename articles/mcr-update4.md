---
title: Insanely Cute Operation
location: moonbase
epoch: 24989
tags: [ MoonCatRescue, Mission ]
---

## Mission Success

After long and arduous debate, the ethereans have collectively decided to allow the Genesis MoonCats to *stay behind on luna.* Attempts to bring the genesis cats onto the spaceship, or even near the transporter pad, proved futile. We have the scratches to prove it. It was clear that the 160 Remaining Genesis MoonCats had made their choice.

<!---MORE--->

While the new specially-fitted sublunarian moonbase has everything 4 billion cats and their descendents will ever need, the Genesis MoonCats determined there was one thing missing. Leaders. The Remaining Genesis MoonCats chose the **path of heroes**, and will stay on the moon to help build out a flourishing MoonCat society. We look forward to checking up on their progress in the years to come.

It was sad saying goodbye, but we know that the Hero Cats are following their hearts. We’re lucky to have the 25,344 rescued MoonCats and brave 96 Genesis MoonCats to care for. As the spaceship’s thrusters engaged, the unrescued mooncats raised their paws in silence, recognizing the courage of the ethereans’ mission. Although years behind schedule and well over budget, our mission was a success. *The final total of 25,440 MoonCats are ready to begin their new lives* on the blockchain.

Back on earth, MoonCat researchers are working tirelessly to understand the MoonCats and how they differ from their earthly siblings. What are their traits? What do they like? What are their kitty dreams made of? Recent breakthroughs in MoonCat linguistics are showing particular promise. *It won’t be long before we gain real insight into their story.*

The 7 brave MoonCats who volunteered for new owners that took on the duty of voting, are suiting up and will be ready in a day or so.

Next Destination: **?**
