---
title: UPDATE 1
location: moonbase
epoch: 24179
tags: [ MoonCatRescue, Mission ]
---

After almost three and a half years we have finally completed construction of the underground moonbase where we carefully designed accommodations for the *billions of MoonCats that we can't take on the journey.* Equipped with the finest cat towers, scratching posts, and an unlimited supply of space tuna, there's no doubt that the unrescued MoonCats will live comfortably here forever.

<!---MORE--->

Upon our return to the surface we found our transactional-sync transporters were down! But also, somehow, *the spaceship was filled to the brim with adopted MoonCats!* The earth ethereum community's spirit proved indomitable, and despite these setbacks, they rose to the challenge. We have no doubt that these rescued MoonCats will enjoy all the ear scratches and belly rubs they can handle for the rest of their precious, pixelated lives.

We immediately set off to resync the transactional-sync transporters. After rerouting the chain through the deflector dish, the communications array at **the adoption center is back online.** We hope our fellow ethereans can now name and trade their MoonCats with ease.
