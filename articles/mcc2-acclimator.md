---
title: The Acclimator
epoch: 31857
tags: [ Acclimator, MoonCat ]
---

The MoonCat ship was met with much fanfare as it docked here at Chainstation Alpha. Over the coming days, however, it became apparent that MoonCats were *not as well adapted* to blockchain life as we had anticipated. Nearly four years had passed since the mission began, and MoonCat *travel technology had not kept up*. So we set to work, as carefully and quickly as we could (hiring MoonCats as consultants did not speed things along as much as we hoped, given their contractual demand for 11 daily nap breaks). However! After many long epochs in the lab, we think our results attest for themselves. We proudly present:

> [The MoonCat Acclimator](/acclimator)

<!---MORE--->

It's a carefully crafted piece of technology which "wraps" the mooncats in an invisible semi-permanent *acclimation bubble.* It can even take those ambitious MoonCats which had already attempted to "wrap" in other ways and **complete their transformation.**

Once acclimated, MoonCats are *perfectly adjusted* to the latest environmental standards (gravity, atmosphere, optimism) and can move much more freely. Not to mention, where previously they would struggle to wear or hold onto anything with their moondust-coated paws, acclimated MoonCats *gain the ability to don or carry* any number of blockchain accoutrements themselves (gone are the days when their friendly Etherean rescuers need to carry all their luggage!).

One more thing. Quite unexpected really. When we flipped the switch the first time we were astonished. At first we thought it must be an error in our sensors. Maybe our eyes. But no, it's true, as a MoonCat acclimates, it begins to **glow.**
