---
title: MCNS
epoch: 71171
tags: [ MoonCatRescue, Ethereans ]
---

After a frantic week, our network team finally tracked down the source of some *unusual traffic patterns.* It turns out the MoonCats have gained access to the uplink and have been sneakily researching Etherean customs (between binges of crypto twitter, which they seem to find irresistible.) When we approached them about it, they ignored our questions and focused on the earth custom of "pet ownership". They purred that the concept was a perfect description of their relationships with Ethereans, and addressed some of their most pressing concerns. In particular: in all the vastness of ChainSpace, *how could they keep track of their Pet Ethereans?*

Deliberations were tense, and it took some time to convey that the majority of Ethereans would *not be happy getting microchipped.* At last a compromise was reached. The engineering team, in collaboration with the Etherean Resources department, worked around the orbit and, having gotten the go-ahead meows, leveraged the ENS system to produce:

> [The MoonCatNameService](/mcns)

<!---MORE--->

Now, with the partial consent of their Ethereans, MoonCats are are able to quickly identify their owners using their seat order number from the rescue mission. For example: `0.ismymooncat.eth` can be used to establish the Etherean that belongs to that (particularly spirited) MoonCat.

Once again, peace on the station has been restored. However brief it will prove to be.


### P. S. A.

Please disregard the previous bulletin, ChainStation Ethereans and Staff are **not required** to report to the MedBay for microchipping. Bulletin system access has now been restricted to level 3 terminals and above.
