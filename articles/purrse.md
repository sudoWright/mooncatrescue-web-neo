---
title: Purrsonal Space
epoch: 85793
tags: [ MoonCatRescue, Moments, ERC998 ]
---

MoonCats love stuff. The acclimation process endowed them with the *capacity to possess ethereal objects* of their own, and a few well timed and gutteral growls have clued us in to **their desire to use it!**

To facilitate this, an enterprising group of MoonCats donned adorable hardhats and set about converting a section of the station's pavillion into a galleria to *show off their favorite possessions and pet Ethereans.* Named in honor of the sounds they made when they completed it, we are pleased to unveil:

> [MoonCatPurrse](//purrse.mooncat.community)

But wait, there's more…

<!---MORE--->

Apparently the MoonCats have been *commemorating special occasions with group photos.* We've assembled a team who is hard at work tracking down these special Moments and getting them into the paws of every MoonCat who participates.

So, to those cats-about-town who have been showing up, you can **expect to see your purrse filling up** with [MoonCatMoments]("/moments").

### P. S. A.

Remember, per Engineering Policy Section E92xff00000ca7: All construction must be approved **in advance** by the ChainStation Structural Integrity team. Failure to adhere to documented approval procedures may result in revokation of station privileges and all of us crashing into the Sun.
